# Base starter template for all new projects.

### Dependancies

- Ruby
- Grunt
- NPM

### Getting Started

git clone git@bitbucket.org:acarpenter/boilerplate.git project-name

cd project-name

npm install

npm install -g grunt-cli

grunt watch 

and start developing. If you have the autoreload browser extension you can get live updates on every save.

