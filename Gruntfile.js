module.exports = function(grunt) {

	// 1. All configuration goes here 
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			dist: {
				options: {
				// cssmin will minify later
				style: 'expanded'
			},
			files: {
				'build/css/style.css': 'styles/global.scss'
				}
			}
		},

		autoprefixer: {
			options: {
				browsers: ['last 2 version']
			},
			single_file: {
				src: 'build/css/style.css',
				dest: 'build/css/style.prefixed.css'
			}
		},

		cssmin: {
			minify: {
				expand: true,
				cwd: 'build/css/',
				src: ['style.prefixed.css'],
				dest: 'build/css/',
				ext: '.min.css'
			}
		},

		concat: {   
			dist: {
				src: ['js/libs/zepto.min.js', 'js/main.js'],
				dest: 'build/js/main.js',
			}
		},

		uglify: {
			build: {
				src: 'build/js/main.js',
				dest: 'build/js/main.min.js'
			}
		},

		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: 'img',
					src: ['*.{png,jpg,gif}'],
					dest: 'build/img'
				}]
			}
		},

		watch: {
			options: {
				livereload: true,
			},
			scripts: {
				files: ['js/*.js', 'js/libs/*.js'],
				tasks: ['concat', 'uglify'],
				options: {
					spawn: false,
				}
			},
			css: {
				files: ['styles/global.scss','styles/ui/*.scss', 'styles/components/*.scss'],
				tasks: ['sass', 'cssmin', 'autoprefixer'],
				options: {
					spawn: false,
				}
			},
			images: {
				files: ['img/**/*.{png,jpg,gif}', 'img/*.{png,jpg,gif}'],
				tasks: ['imagemin'],
				options: {
					spawn: false,
				}
			}
		}

	});

	// 3. Where we tell Grunt we plan to use this plug-in.
	require('load-grunt-tasks')(grunt);

	// 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
	grunt.registerTask('default', ['sass', 'autoprefixer', 'cssmin', 'concat', 'uglify', 'imagemin']);
	grunt.registerTask('build', ['sass', 'autoprefixer', 'cssmin', 'concat', 'uglify', 'imagemin']);

};